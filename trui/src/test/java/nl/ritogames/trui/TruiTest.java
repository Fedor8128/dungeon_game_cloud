package nl.ritogames.trui;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Unit test for simple App.
 */
public class TruiTest
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        Trui trui = new Trui();
        assertTrue( trui.testMethod() );
    }
}