package nl.ritogames.networkhandler

import nl.ritogames.shared.dto.{ChatMessage, GameStatePatch, InteractionEvent}
import nl.ritogames.shared.{IChatMessageHandler, IEventSender, IGameStateDistributor, IMessageSender}

object NetworkHandler extends IEventSender with IMessageSender with IGameStateDistributor {
  override def sendEvent(event: InteractionEvent): Unit = ???

  override def sendMessage(message: ChatMessage): Unit = ???

  override def registerChatMessageHandler(chatMessageHandler: IChatMessageHandler): Unit = ???

  override def distributeState(gameStateDiff: GameStatePatch): Unit = ???
}