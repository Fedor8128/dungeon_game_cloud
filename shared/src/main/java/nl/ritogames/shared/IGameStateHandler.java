package nl.ritogames.shared;

import nl.ritogames.shared.dto.Character;
import nl.ritogames.shared.dto.GameStateContext;
import nl.ritogames.shared.dto.GameStatePatch;

public interface IGameStateHandler {
    GameStateContext getGameStateContext(Character character);

    void patchGameState(GameStatePatch gameStatePatch);


}
