package nl.ritogames.shared;

import nl.ritogames.shared.dto.InteractionEvent;

public interface IEventSender {
    void sendEvent(InteractionEvent event);
}
