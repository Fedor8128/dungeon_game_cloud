package nl.ritogames.shared;

import nl.ritogames.shared.dto.ChatMessage;

public interface IMessageSender {
    void sendMessage(ChatMessage message);

    void registerChatMessageHandler(IChatMessageHandler chatMessageHandler);
}
