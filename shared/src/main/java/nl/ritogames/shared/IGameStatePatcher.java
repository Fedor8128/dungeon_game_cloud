package nl.ritogames.shared;

import nl.ritogames.shared.dto.GameStatePatch;

@FunctionalInterface
public interface IGameStatePatcher {
    void processStateDiff(GameStatePatch gameStatePatch);
}
