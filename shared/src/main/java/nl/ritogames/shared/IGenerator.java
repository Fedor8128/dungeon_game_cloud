package nl.ritogames.shared;

import nl.ritogames.shared.dto.World;

public interface IGenerator {
    World generateWorld();

    void generateItems(World world);

    void generateMonsters(World world);
}
