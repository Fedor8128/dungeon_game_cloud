package nl.ritogames.shared;

import nl.ritogames.shared.exception.ParseInputException;

public interface IParser {
    void parseInput(String input) throws ParseInputException;
}
