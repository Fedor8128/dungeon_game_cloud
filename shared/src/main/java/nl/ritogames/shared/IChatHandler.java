package nl.ritogames.shared;

import nl.ritogames.shared.dto.GameChat;

@FunctionalInterface
public interface IChatHandler {
    void onChatChanged(GameChat gameChat);
}
