package nl.ritogames.shared;

import nl.ritogames.shared.dto.ChatMessage;

@FunctionalInterface
public interface IChatMessageHandler {
    void onChatReceived(ChatMessage chatMessage);
}
