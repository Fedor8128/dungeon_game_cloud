package nl.ritogames.shared;

public enum Direction {
    UP,
    DOWN,
    LEFT,
    RIGHT
}
