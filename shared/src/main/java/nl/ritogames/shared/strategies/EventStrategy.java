package nl.ritogames.shared.strategies;

import nl.ritogames.shared.dto.GameStatePatch;
import nl.ritogames.shared.dto.InteractionEvent;

public abstract class EventStrategy {
    public InteractionEvent event;

    public abstract void validateEvent(InteractionEvent event);
    public abstract GameStatePatch processEvent();
}
