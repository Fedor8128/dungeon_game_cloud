package nl.ritogames.shared;

import nl.ritogames.shared.dto.InteractionEvent;

public interface IEventHandler {
    void handleEvent(InteractionEvent event);
}
