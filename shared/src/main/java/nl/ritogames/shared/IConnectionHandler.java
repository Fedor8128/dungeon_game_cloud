package nl.ritogames.shared;

@FunctionalInterface
public interface IConnectionHandler {
    void onConnectionLost();
}
