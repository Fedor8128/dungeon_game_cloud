package nl.ritogames.shared;

public interface IChat {
    boolean sendMessage(String message);

    void registerChatChangedHandler(IChatHandler chatHandler);
}
