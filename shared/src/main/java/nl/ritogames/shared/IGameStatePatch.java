package nl.ritogames.shared;

public interface IGameStatePatch {
    void registerStateDiffHandler(IGameStatePatcher gameStateDiffHandler);
}
