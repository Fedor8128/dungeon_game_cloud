package nl.ritogames.shared;

import nl.ritogames.shared.dto.GameStatePatch;
import nl.ritogames.shared.exception.StateDistributionException;

public interface IGameStateDistributor {
    void distributeState(GameStatePatch gameStatePatch) throws StateDistributionException;
}
