package nl.ritogames.shared;

import nl.ritogames.shared.exception.CompilationException;

import java.io.File;

public interface IAgentCompiler {
    File compile(File source) throws CompilationException;
}
