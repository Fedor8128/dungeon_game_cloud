package nl.ritogames.shared.dto;

import nl.ritogames.shared.Direction;

import java.awt.*;

public class Character {
    private final Dimension location;
    private int hp;

    public Character(int x, int y) {
        hp = 100;
        location = new Dimension(x, y);
    }

    public int damage(int damage) {
        return hp -= damage;
    }

    public int heal(int hp) {
        return this.hp += hp;
    }

    public Dimension move(Direction direction) {
        switch (direction) {
            case UP -> location.height++;
            case DOWN -> location.height--;
            case RIGHT -> location.width++;
            case LEFT -> location.width--;
        }
        return location;
    }
}
