package nl.ritogames.shared.dto;

import java.util.List;

public class GameState {
    private List<Character> characters;
    private World world;

    public List<Character> getCharacters() {
        return characters;
    }

    public void addCharacter(Character character) {
        characters.add(character);
    }

    public World getWorld() {
        return world;
    }
}
