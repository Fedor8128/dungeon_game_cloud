package nl.ritogames;

import nl.ritogames.interfaces.IGameStatePatcher;
import nl.ritogames.shared.dto.GameStatePatch;
import nl.ritogames.shared.dto.Packet;

public class GameStatePatcher implements IGameStatePatcher {
    private PatchApplier applier;

    @Override
    public void setModificationApplier(PatchApplier applier) {
        this.applier = applier;
    }

    @Override
    public void extractPatch(Packet packet) {

    }

    private void applyPatch(GameStatePatch patch) {

    }
}
