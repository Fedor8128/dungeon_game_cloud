package nl.ritogames.interfaces;

import nl.ritogames.PatchApplier;
import nl.ritogames.shared.dto.Packet;

public interface IGameStatePatcher {
    void setModificationApplier(PatchApplier applier);
    void extractPatch(Packet packet);
}
