package nl.ritogames.interfaces;

import nl.ritogames.PatchApplier;
import nl.ritogames.shared.dto.InteractionEvent;
import nl.ritogames.shared.strategies.EventStrategy;

public interface IGameStateModifier {
    void setModificationApplier(PatchApplier applier);
    void processEvent(EventStrategy strategy);
}
