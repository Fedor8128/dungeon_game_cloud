package nl.ritogames;

import nl.ritogames.interfaces.IGameStateModifier;
import nl.ritogames.shared.dto.GameStatePatch;
import nl.ritogames.shared.dto.InteractionEvent;
import nl.ritogames.shared.strategies.EventStrategy;

public class GameStateModifier implements IGameStateModifier {
    private PatchApplier applier;
    private GameStatePatch patch;

    @Override
    public void setModificationApplier(PatchApplier applier) {
        this.applier = applier;
    }

    @Override
    public void processEvent(EventStrategy strategy) {

    }
}
